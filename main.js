/**
 * Starting work of page
 */
function pageStart() {
  console.log("Page script starting");
  const tableHeaders = [
    ["id", "Название", "Управление"],
    ["id", "Имя", "Вид", "Управление"],
  ];

  getTableContent("type", tableHeaders[0]);
  getTableContent("animal", tableHeaders[1]);
  refreshTA();
}

const getTableContent = (tableName, headers) => {
  fetch(`api/${tableName}`)
    .then((res) => res.json())
    .then((data) => {
      const tableContent = data.data;
      console.log(tableContent);

      // Table head and body
      createTable(tableName, headers);

      // Table content
      if (tableName == "animal") {
        fetch("api/type", {
          method: "GET",
          redirect: "follow",
        })
          .then((response) => response.json())
          .then((result) => {
            //console.log(result);
            for (let line = 0; line < tableContent.length; line++) {
              appendTableRow(tableName, tableContent[line], result);
            }
          })
          .catch((error) => console.log("error", error));
      } else {
        for (let line = 0; line < tableContent.length; line++) {
          //console.log(line)
          appendTableRow(tableName, tableContent[line]);
        }
      }
    });
};

const refreshTable = (tableName) => {
  fetch(`api/${tableName}`)
    .then((res) => res.json())
    .then((data) => {
      const tableContent = data.data;
      //console.log(tableContent);

      // Table head and body
      //createTable(tableName, headers);
      const tableBody = document.querySelector(`tbody.tbody-${tableName}`);
      while (tableBody.firstChild) {
        tableBody.removeChild(tableBody.firstChild);
      }
      // Table content
      for (let line = 0; line < tableContent.length; line++) {
        //console.log(line)
        appendTableRow(tableName, tableContent[line]);
      }
    });
};

/**
 * Creates empty table with TH line of headers and "Add" button
 * @param {*} tableName
 * @param {*} headers
 */
const createTable = (tableName, headers) => {
  console.log(`Creation start "${tableName}"`);
  const tableDiv = document.querySelector(`div.${tableName}`);

  // clean-up
  while (tableDiv.firstChild) {
    tableDiv.removeChild(tableDiv.firstChild);
  }

  const table = document.createElement("table");
  table.className = `table-${tableName}`;

  // THead
  const tableHead = document.createElement("thead");
  tableHead.className = `thead-${tableName}`;

  // THead row
  const tableHeaderRow = document.createElement("tr");
  tableHeaderRow.className = `thead-row-${tableName}`;

  // THead row content
  headers.forEach((element) => {
    let currentTH = document.createElement("th");
    currentTH.innerText = element;

    if (element == "control" || element == "Управление") {
      let buttonAdd = document.createElement("button");
      currentTH.append(buttonAdd);
      buttonAdd.className = "addRow button";
      buttonAdd.id = `addRow-${tableName}`;
      buttonAdd.innerText = "Добавить";
      buttonAdd.onclick = (e) => {
        addNewRow(tableName);
      };
    }

    tableHeaderRow.append(currentTH);
  });

  // Append elements
  tableHead.append(tableHeaderRow);
  table.append(tableHead);

  const tableBody = document.createElement("tbody");
  tableBody.className = `tbody-${tableName}`;
  tableBody.id = `dynamic-${tableName}`;
  table.append(tableBody);

  tableDiv.append(table);
  console.log(`Creation complete: "${tableName}"`);
};

const addNewRow = (tableName) => {
  console.log(`Add row: "${tableName}"`);
  let content = {};

  // Input window
  if (tableName == "type") {
    // overlay
    let overlay = document.getElementById("overlay-modal");
    overlay.style.opacity = 1;
    overlay.style.visibility = "visible";

    // modal window
    let dialog = document.getElementById("addNewType");
    dialog.style.opacity = 1;
    dialog.style.visibility = "visible";

    // input
    let inputNewType = document.getElementById("inputNewType");

    // buttons
    let buttonOk = document.getElementById("newTypeOk");
    buttonOk.onclick = () => {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      let raw = JSON.stringify({
        type_name: inputNewType.value,
      });

      let requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch("api/type", requestOptions)
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log("error", error));

      // set content for table row
      content.type_name = inputNewType.value;

      // hide modal menu
      inputNewType.textContent = "";
      overlay.style.opacity = 0;
      overlay.style.visibility = "hidden";
      dialog.style.opacity = 0;
      dialog.style.visibility = "hidden";
      refreshTable(tableName);
      refreshTA();
    };

    let buttonCancel = document.getElementById("newTypeCancel");
    buttonCancel.onclick = () => {
      // hide modal menu
      inputNewType.textContent = "";
      overlay.style.opacity = 0;
      overlay.style.visibility = "hidden";
      dialog.style.opacity = 0;
      dialog.style.visibility = "hidden";
    };
  }

  if (tableName == "animal") {
    // overlay
    let overlay = document.getElementById("overlay-modal");
    overlay.style.opacity = 1;
    overlay.style.visibility = "visible";

    // modal window
    let dialog = document.getElementById("addNewAnimal");
    dialog.style.opacity = 1;
    dialog.style.visibility = "visible";

    // input
    let inputNewAnimal = document.getElementById("inputNewAnimal");
    let $selectType = document.getElementById("selectType");
    $selectType.innerHTML = "";

    // select content
    let requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch("api/type", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        //console.log(result);
        result.data.forEach((el) => {
          //console.log(el);
          let newOption = new Option(el.type_name, el.type_id);
          $selectType.options[$selectType.options.length] = newOption;
        });
      })
      .catch((error) => console.log("error", error));

    // buttons
    let buttonOk = document.getElementById("newAnimalOk");
    buttonOk.onclick = () => {
      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      let raw = JSON.stringify({
        animal_name: inputNewAnimal.value,
        animal_type: $selectType.value,
      });

      let requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      fetch("api/animal", requestOptions)
        .then((response) => response.text())
        .then((result) => console.log(result))
        .catch((error) => console.log("error", error));

      // set content for table row
      content.animal_name = inputNewAnimal.value;
      content.animal_type = $selectType.value;

      checkLostPet(inputNewAnimal.value, $selectType.value);
      // hide modal menu
      inputNewAnimal.textContent = "";
      overlay.style.opacity = 0;
      overlay.style.visibility = "hidden";
      dialog.style.opacity = 0;
      dialog.style.visibility = "hidden";
      refreshTable(tableName);
      refreshTA();
    };

    let buttonCancel = document.getElementById("newAnimalCancel");
    buttonCancel.onclick = () => {
      // hide modal menu
      inputNewAnimal.textContent = "";
      overlay.style.opacity = 0;
      overlay.style.visibility = "hidden";
      dialog.style.opacity = 0;
      dialog.style.visibility = "hidden";
    };
  }
};

const appendTableRow = async (tableName, content) => {
  const table = document.querySelector(`.tbody-${tableName}`);
  const row = document.createElement("tr");
  row.className = `tRow-${tableName}`;
  const id = document.createElement("td");
  const name = document.createElement("td");

  if (tableName == "animal") {
    // TODO CROsS TABLE DELETION

    id.innerText = content.animal_id;
    name.innerText = content.animal_name;
    const type = document.createElement("td");

    // get list of types
    let correctType = "deleted";

    fetch("api/type", {
      method: "GET",
      redirect: "follow",
    })
      .then((response) => response.json())
      .then((result) => {
        for (let i = 0; i < result.data.length; i++) {
          if (result.data[i].type_id == content.animal_type)
            correctType = result.data[i].type_name;
        }
        if (correctType == "deleted") {
          deleteRecord(content.animal_id, "animal");
          refreshAll();
          return;
        } else {
          type.innerText = correctType;
        }
      })
      .catch((error) => console.log("error", error));
    row.append(id, name, type);
    row.id = `${content.animal_id}`;
  }

  if (tableName == "type") {
    id.innerText = content.type_id;
    name.innerText = content.type_name;
    row.append(id, name);
    row.id = `${content.type_id}`;
  }

  // Buttons
  const buttonsTD = document.createElement("td");
  buttonsTD.className = "buttons";

  // Edit button
  const buttonEdit = document.createElement("button");
  buttonEdit.className = "edit button";
  buttonEdit.innerText = "Изменить";
  buttonEdit.onclick = (e) => {
    editRecord(e, tableName, content);
  };

  // Delete button
  const buttonDelete = document.createElement("button");
  buttonDelete.className = "delete button";
  buttonDelete.innerText = "Удалить";
  buttonDelete.onclick = (e) => {
    deleteRecord(e.path[2].id, tableName);
  };

  // Append section
  buttonsTD.append(buttonEdit, buttonDelete);
  row.append(buttonsTD);
  table.append(row);
};

/**
 *
 * @param {*} id ID in DB
 * @param {*} tableName
 */
const deleteRecord = (id, tableName) => {
  console.log(`Deleting row with id ${id}`);
  fetch(`/api/${tableName}/${id}`, {
    method: "DELETE",
    redirect: "follow",
  })
    .then((response) => response.text())
    .then((result) => console.log(result))
    .catch((error) => console.log("error", error));

  refreshAll();
};

const editRecord = (e, tableName, content) => {
  // get ID of row, which equal actual ID in DB
  //console.log(e.path[2].id);

  let $overlay = document.getElementById("overlay-modal");
  $overlay.style.opacity = 1;
  $overlay.style.visibility = "visible";

  // modal window
  let $dialog = document.getElementById(`edit${tableName}`);
  $dialog.style.opacity = 1;
  $dialog.style.visibility = "visible";

  // input
  let $inputEdit = document.getElementById(`inputEdit${tableName}`);
  let $buttonOk = document.getElementById(`edit${tableName}Ok`);
  let $buttonCancel = document.getElementById(`edit${tableName}Cancel`);
  let $selectEditType = document.getElementById("selectEditType");

  // clear all previous options
  $selectEditType.innerHTML = "";

  if (tableName == "animal") {
    // select content
    let requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    fetch("api/type", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        //console.log(result);
        result.data.forEach((el) => {
          //console.log(el);
          let newOption = new Option(el.type_name, el.type_id);
          $selectEditType.options[$selectEditType.options.length] = newOption;
        });
      })
      .catch((error) => console.log("error", error));
  }

  $buttonOk.onclick = () => {
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    if (tableName == "type") {
      raw = JSON.stringify({
        type_name: $inputEdit.value,
      });
    }

    if (tableName == "animal") {
      raw = JSON.stringify({
        animal_name: $inputEdit.value,
        animal_type: $selectEditType.value,
      });
    }

    let requestOptions = {
      method: "PUT",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(`api/${tableName}/${e.path[2].id}`, requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));

    // set content for table row
    if (tableName == "type") {
      content.type_name = $inputEdit.value;
    }

    if (tableName == "animal") {
      content.animal_type = $selectEditType.value;
      content.animal_name = $inputEdit.value;
    }

    // hide modal menu
    $inputEdit.value = "";
    $overlay.style.opacity = 0;
    $overlay.style.visibility = "hidden";
    $dialog.style.opacity = 0;
    $dialog.style.visibility = "hidden";
    refreshAll();
  };

  $buttonCancel.onclick = () => {
    // hide modal menu
    $inputEdit.value = "";
    $overlay.style.opacity = 0;
    $overlay.style.visibility = "hidden";
    $dialog.style.opacity = 0;
    $dialog.style.visibility = "hidden";
    refreshAll();
  };
};

const refreshTA = () => {
  fetch(`api/type`).then((response) => {
    response.text().then((text) => {
      document.querySelector(`#typeTA`).textContent = text;
    });
  });

  fetch(`api/animal`).then((response) => {
    response.text().then((text) => {
      document.querySelector(`#animalTA`).textContent = text;
    });
  });
};

const refreshAll = () => {
  refreshTable("animal");
  refreshTable("type");
  refreshTA();
};

// report find

const $btnReportSet = document.getElementById("reportSet");
const $btnReportSave = document.getElementById("reportSave");
const $reportName = document.getElementById("reportInput");
const $reportType = document.getElementById("reportSelection");

$btnReportSet.onclick = () => {
  $btnReportSet.disabled = true;
  $btnReportSave.disabled = false;

  $reportName.disabled = false;
  $reportType.disabled = false;

  $reportType.innerHTML = "";
  fetch("api/type", {
    method: "GET",
    redirect: "follow",
  })
    .then((response) => response.json())
    .then((result) => {
      result.data.forEach((el) => {
        let newOption = new Option(el.type_name, el.type_id);
        $reportType.options[$reportType.options.length] = newOption;
      });
    })
    .catch((error) => console.log("error", error));
};

$btnReportSave.onclick = () => {
  $btnReportSet.disabled = false;
  $reportName.disabled = true;
  $reportType.disabled = true;
};

const checkLostPet = (name, type) => {
  if (
    name == $reportName.value &&
    type == $reportType.selectedOptions[0].value
  ) {
    console.log("Pet found!");

    let report = window.open("");

    report.document.write(
      `<h1>Было найдено животное</h1>
      <h3>Имя: ${name}</h3>
      <h3>Вид: ${$reportType.selectedOptions[0].innerText}</h3>`
    );
    report.stop();
    report.print();
    report.close();
  }
};

pageStart();
