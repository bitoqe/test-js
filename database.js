let sqlite3 = require("sqlite3").verbose();

const DBSOURCE = "db.sqlite";

let db = new sqlite3.Database(DBSOURCE, (err) => {
  if (err) {
    // Cannot open database
    console.error(err.message);
    throw err;
  } else {
    console.log("Connected to the SQLite database.");
    db.run(
      `CREATE TABLE "type" (
        "type_id"	INTEGER NOT NULL UNIQUE,
        "type_name"	TEXT NOT NULL UNIQUE,
        PRIMARY KEY("type_id" AUTOINCREMENT)
      )`,
      (err) => {
        if (err) {
          // Table already created
          console.log("TYPE table already exist");
        } else {
          // Table just created, creating some rows
          var insert = "INSERT INTO type (type_name) VALUES (?)";
          db.run(insert, "cat");
          db.run(insert, "dog");
          console.log("TYPE table filled with default data");
        }
      }
    );
    db.run(
      `CREATE TABLE "animal" (
        "animal_id"	INTEGER NOT NULL UNIQUE,
        "animal_type"	NUMERIC NOT NULL,
        "animal_name"	TEXT NOT NULL,
        PRIMARY KEY("animal_id" AUTOINCREMENT),
        CONSTRAINT "fk_type" FOREIGN KEY("animal_type") REFERENCES "type"("type_id") ON DELETE CASCADE
      )`,
      (err) => {
        if (err) {
          // Table already created
          console.log("ANIMAL table already exist");
        } else {
          // Table just created, creating some rows
          var insert =
            "INSERT INTO animal (animal_type, animal_name) VALUES (?,?)";
          db.run(insert, [1, "alpha"]);
          db.run(insert, [2, "beta"]);
          console.log("ANIMAL table filled with default data");
        }
      }
    );
  }
});
module.exports = { db };
