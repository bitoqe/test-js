const { json, urlencoded } = require("express");
const express = require("express");
const app = express();
const HTTP_PORT = 8000;
let db = require("./database.js");
const path = require("path");

app.listen(HTTP_PORT, () => {
  console.log(`express on ${HTTP_PORT}`);
});

app.use(json());
app.use(urlencoded({ extended: false }));

app.use(express.static(__dirname + "/")); // add style.css

app.get("/", (req, res, next) => {
  res.sendFile(path.join(__dirname, "./index.html"));
  //res.json({ message: "ok" });
});

/*
==================== GET ====================
*/
app.get("/api/type", (req, res, next) => {
  let sql = "SELECT * FROM type";
  let params = [];
  db.db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({
        error: err.message,
      });
      return;
    }
    res.json({
      message: "success",
      data: rows,
    });
  });
  console.log("GET all: /api/type");
});

app.get("/api/animal", (req, res, next) => {
  var sql = "SELECT * FROM animal";
  var params = [];
  db.db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: "success",
      data: rows,
    });
  });
  console.log("GET all: /api/animal");
});

app.get("/api/animal/:id", (req, res, next) => {
  var sql = "SELECT * FROM animal WHERE animal_id = ?";
  var params = [req.params.id];
  db.db.get(sql, params, (err, row) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: "success",
      data: row,
    });
  });
  console.log("GET single: /api/animal/:id");
});

app.get("/api/type/:id", (req, res, next) => {
  var sql = "SELECT * FROM type WHERE type_id = ?";
  var params = [req.params.id];
  db.db.get(sql, params, (err, row) => {
    if (err) {
      res.status(400).json({ error: err.message });
      return;
    }
    res.json({
      message: "success",
      data: row,
    });
  });
  console.log("GET single: /api/type/:id");
});

/*
==================== POST ====================
*/

app.post("/api/type/", (req, res, next) => {
  // check errors
  let errors = [];
  if (!req.body.type_name) {
    errors.push("No type_name specified");
  }
  if (errors.length) {
    res.status(400).json({
      error: errors.join(","),
    });
    return;
  }

  let data = {
    type_name: req.body.type_name,
  };
  let sql = "INSERT INTO type (type_name) VALUES (?)";
  let params = [data.type_name];
  db.db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({
        error: err.message,
      });
      return;
    }
    res.json({
      message: "success",
      data: data,
      id: this.lastID,
    });
  });
  console.log("POST single: /api/type/");
});

app.post("/api/animal/", (req, res, next) => {
  // check errors
  let errors = [];
  if (!req.body.animal_name) {
    errors.push("No animal_name specified");
  }
  if (!req.body.animal_type) {
    errors.push("No animal_type specified");
  }
  if (errors.length) {
    res.status(400).json({
      error: errors.join(","),
    });
    return;
  }

  let data = {
    animal_name: req.body.animal_name,
    animal_type: req.body.animal_type,
  };
  let sql = "INSERT INTO animal (animal_name, animal_type) VALUES (?,?)";
  let params = [data.animal_name, data.animal_type];
  db.db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({
        error: err.message,
      });
      return;
    }
    res.json({
      message: "success",
      data: data,
      id: this.lastID,
    });
  });
  console.log("POST single: /api/animal/");
});

/*
==================== PUT ====================
*/
app.put("/api/animal/:id", (req, res, next) => {
  let data = {
    animal_name: req.body.animal_name,
    animal_type: req.body.animal_type,
  };
  let params = [data.animal_name, data.animal_type, req.params.id];
  let sql =
    "UPDATE animal SET animal_name = ?, animal_type = ? WHERE animal_id = ?";
  db.db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: res.message });
      console.log(err);
      return;
    }
    res.json({
      message: "success",
      data: data,
      changes: this.changes,
    });
  });
  console.log("PUT single: /api/animal/:id");
});

app.put("/api/type/:id", (req, res, next) => {
  let data = {
    type_name: req.body.type_name,
  };
  let params = [data.type_name, req.params.id];
  let sql = "UPDATE type SET type_name = ? WHERE type_id = ?";
  db.db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: res.message });
      console.log(err);
      return;
    }
    res.json({
      message: "success",
      data: data,
      changes: this.changes,
    });
  });
  console.log("PUT single: /api/type/:id");
});

/*
==================== DELETE ====================
*/
app.delete("/api/type/:id", (req, res, next) => {
  let data = {};
  let params = [req.params.id];
  let sql = "DELETE FROM type WHERE type_id = ?";
  db.db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: res.message });
      return;
    }
    res.json({ message: "deleted", changes: this.changes });
  });
  console.log("DELETE single: /api/type/:id");
});

app.delete("/api/animal/:id", (req, res, next) => {
  let data = {};
  let params = [req.params.id];
  let sql = "DELETE FROM animal WHERE animal_id = ?";
  db.db.run(sql, params, function (err, result) {
    if (err) {
      res.status(400).json({ error: res.message });
      return;
    }
    res.json({ message: "deleted", changes: this.changes });
  });
  console.log("DELETE single: /api/animal/:id");
});

app.use((req, res) => {
  res.status(404);
  res.json({ message: "404 OOPS" });
});
