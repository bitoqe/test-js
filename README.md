# Программа регистрация животных, поступивших в приют

### Запуск 
0. загрузить репозиторий
0. перейти в каталог репозитория
0. выполнить команду `$ npm i` для установки зависимостей (необходим node js)
0. запустить работу локального сервера - `$ npm start`
0. перейти в браузер на адрес `http://localhost:8000/`

### Планируемый стек технологий:

1. python:
   - python + tkinter
   - sqlite
   - [VSCode]
2. JS:
   - [JavaScript] + [Node.js] + [Express.js]
   - [sqlite3]
   - [VSCode]

### Использованный стек технологий:

- [JavaScript] + [Node.js] + [Express.js]
- [VSCode]
- [Prettier]

### Использованные расширения node.js:

- [Express.js]
- [sqlite3]
- [nodemon]

### Файлы:

```sh
.
├── database.js         - подключение к базе, создание таблиц, если отсутствуют
├── db.sqlite           - сама БД
├── index.html
├── main.js             - основной функционал приложения
├── node_modules
├── package.json
├── package-lock.json
├── README.md
├── server.js           - запуск и настройка сервера на express.js. Параметры api
└── style.css
```

[javascript]: (https://developer.mozilla.org/ru/docs/Web/JavaScript)
[node.js]: (https://nodejs.org/en/)
[electron.js]: (https://www.electronjs.org/)
[express.js]: (https://expressjs.com/)
[prettier]: (https://prettier.io/)
[vscode]: (https://code.visualstudio.com/)
[db browser for sqlite]: (https://sqlitebrowser.org/)
[sqlite3]: (https://www.npmjs.com/package/sqlite3)
[nodemon]: (https://www.npmjs.com/package/nodemon)
